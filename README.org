# lwf_userfriendly_cgroup

This is a Slackware-specific package for doing things outlined in the design document:
https://gitlab.com/Lockywolf/linuxbugs-lwf/blob/master/doc/Reading-cgroups-manual.txt

It's aimed at optimizing resource usage.

* Development aims

** Restrict memory and cpu for heavy applications, also give them less priority: Firefox, Chrome and such.
** Give higher responsiveness to the GUI: guarantee cpu time, memory (not implemented), prohibit swapping.
** Provide maximum survivability to system apps: guarantee cpu time, guarantee memory (not implemented), permit swapping and give higher priority.
** Achieve all of that with stock Slackware-current tools only.
** Require zero configuration. Install the package -- you get it working. Uninstall -- the system looks untouched.
** Ideally, make this package cross-system in a sense that even if you don't have pkgtools you would be able to just unpack the tgz into the system root.

* Known issues
** The startup time is a bit slow
This is due to many potential apps in the lists. You can edit the default application lists in /etc/cgrules.templates/ to make them shorter.
** Many undiscovered bugs

* Configuration files
** /etc/rc.d/rc.lwf_userfriendly_cgroup.conf
This is the file where the restrictions are set.
There you can set whether you want to load preconfigure group process lists (LOAD_LWF_SLACKWARE_LISTS), use simple custom lists(LOAD_CUSTOM_LISTS), or try to write everything yourself (LOAD_HEURISTICS).
This file is sourced, so you can use all bash syntax you want.
Variables AWK_* will be used when generating the actual control group restrictions.
Of those there are four:
***system (only cpu guaranteed)
***multimedia (cpu guaranteed and restricted)
***GUI (cpu guaranteed, no swapping)
***restricted (cpu restricted, memory restricted)

** /etc/cgrules.templates/z_custom*
Are simple executable file lists.

** /etc/cgrules.templates/zz_heuristic
By default contains some simply pasted lists of applications I particularly want to limit.
It is sourced, write any bash code you want there.

* Compatibility
The rc.lwf_userfriendly_cgroup tries to respect custom-configured cgroups ruled by rc.cgconfig and rc.cgred , but due to the sheer intrusiveness of this configuration, nothing is guaranteed.

* Installation
Ideally, just installing the package if you use a Slackware system is enough.

For non-Slackware systems:
** Unpack the source
** Copy the src/etc directory over your /etc/
** Rename /etc/rc.lwf_userfriendly_cgroup.conf.new /etc/rc.lwf_userfriendly_cgroup.conf
** Rename all /etc/cgrules.templates/z_custom*.new to /etc/cgrules.templates/z_custom*
** Rename /etc/cgrules.templates/zz_heuristic.new to /etc/cgrules.templates/zz_heuristic
** Add "/etc/rc.d/rc.lwf_userfriendly_cgroup start"" to your init system. (Distribution-dependent)

If your system uses some particularly cool runlevel configuration, you may want to adjust your rc.local, rc.local_shutdown and rc{1,2,3,4,5}.d directories, but that depends on the particulars of your distribution.

* TODO
** blkio for Firefox and Skype, and dd. Clamp everyone to ~90% I/O.
** cpuacct monitoring
** /sys/fs/cgroup/perf_event
** There is still a small lag between starting an input and the letters appearing.
I sort of know how to track it. Not very annoying, but still. Perhaps, bash-completion should help.
