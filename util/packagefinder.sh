#!/bin/bash
#find /var/log/packages/ -regextype egrep -regex ".*($(find . ! -path . ! -path .. -type d | tr -d './' | tr '\n' '|'|head --bytes -1 ))+.*"

#find /var/log/packages/ -regextype egrep -regex ".*($(find . ! -path . ! -path .. -type d | tr -d './' | tr '\n' '|'|head --bytes -1 ))+.*" -exec cat {} \; | grep -E -e "lib64|bin"

# $1 must be a file name, $2 must be dirs or files to check

set -euo pipefail

filelist=$(find /var/log/packages/ -regextype egrep -regex "/var/log/packages/($(find . ! -path . -maxdepth 1 ! -path .. -type $2 | tr -d './' | tr '\n' '|' | sed 's/|/-|/g' |head --bytes -1 ))+.*" -exec cat {} \; | grep '/' | grep -v 'PACKAGE LOCATION' | grep -v '\./' )
for pth in $filelist
do
    printf "$pth\n"
    if [ -f "/$pth" -a -x "/$pth" ]; then
       printf "/$pth\n" >> $1_binary_list
    fi
done

